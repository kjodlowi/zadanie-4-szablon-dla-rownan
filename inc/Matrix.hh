#pragma once

#include "Vector.hh"
#include <iostream>
#include <iomanip>

/*
* Klasa Matrix bazuje na wektorze składającym się z Size elementow typu Type.
* Dostęp do tablicy _tab(i,j) odbywa się przez przeciezenie
* operatora (), co pozwala na zapisywanie i odczytywanie danych.
*/
template <typename Type, int Size>
class Matrix
{
  Vector<Type, Size> _tab[Size];
public:

  /* metoda, która zwraca wartosc przypisana do
  * elementu [n][m] macierzy 
  */
  Type operator()(int n, int m) const { return _tab[n](m); }

  /* metoda, która zamienia wartosc elementu [n][m] macierzy 
  * na wartosc typu float - elem
  */
  Type & operator()(int n, int m)  { return _tab[n](m);  }


};

/*! Przeciążenie operatora >>
* Agrumenty:
* - stream - strumień, z którego wczytujemy dane
* - matrix - macierz, do któregj wczytujemy dane
*
* Zapisuje dane w postaci zmiennych typu dobule do tablicy 
* macierzy matrix.
*/
template <typename Type, int Size>
std::istream &operator>>(std::istream &stream, Matrix<Type, Size> &matrix)
{
    for (int i=0;i < Size;i++)
    {
        for (int j = 0; j < Size;j++)
        {
           std::cin >> matrix(j,i);
        }
    }
    return stream;
}

/*! Przeciążenie operatora <<
* Agrumenty:
* - stream - strumień, na który wypisujemy dane
* - matrix - macierz, na która wypisujemy dane
*
* Wypisuje zmienne z tablicy macierzy matrix na strumień stream. 
*/
template <typename Type, int Size>
std::ostream &operator<<(std::ostream &stream, const Matrix<Type, Size> &matrix)
{
    for (int i=0;i < Size;i++)
    {
        for (int j = 0; j < Size;j++)
        {
            std::cout << matrix(j,i) << "  ";
        }
        std::cout << std::endl;
    }
    return stream;    
}


/*! Wyliczenie wyznacznika macierzy.
 * Agrument:
 * - matrix - macierz, której wyznacznik chemy policzyć
 * Zwraca: 
 * Wartość typu Type, która jest wyznacznikiem macierzy matrix.
 */
template <typename Type, int Size>
Type determinant(Matrix<Type, Size> matrix)
{
    int n = 0, m = 0, j = 1;
    Type tmp;

    do 
    {
        if (matrix(n,n) != 0)
        {
            j = 1;

            while (n + j < Size)
            {
                tmp = matrix(n+j,n) / matrix(n,n);
                while (m < Size)
                {
                    matrix(n+j,m) -= matrix(n,m) * tmp;
                    m++;
                }
                m = 0;
                j++;
            }
            n++;
            j = 1;
            m = 0;
        }else
        {
            while (m < Size)
            {
                tmp = matrix(n,m);
                matrix(n,m) = matrix(n+j,m);
                matrix(n+j,m) = tmp;
                m++;
            }
            j++;
            if (n+j >= Size)
            {
                if (matrix(n,n) != 0)
                {
                    n++;
                }else
                {
                    set(tmp,0);
                    n = Size + 1;
                }
                
            }
        }
    }while (n < Size);

    set(tmp,1);
    n = -1;
    while (n < Size -1)
    {
       n++; 
       tmp *= matrix(n,n);
    }

    return tmp;
}

/*! Przeciążenie operatora * 
* Arguemnty:
* - matrix - macierz, którą mnożymy
* - vec - wektor, przez który mnożymy macierz
* Zwraca:
* Wektor, który jest wynikiem mnożenia.
*/
template <typename Type, int Size>
Vector<Type, Size> operator*(const Matrix<Type, Size> matrix, const Vector<Type, Size> vec)
{
    Vector<Type, Size> result;
    Type tmp(0);

    for (int i = 0; i < Size; i++)
    {
        set(tmp,0);
        for (int j = 0; j < Size; j++)
        {
            tmp += matrix(i,j) * vec(j);
        }
        result(i) = tmp;
    }
    return result;
}

/*! Przeciążenie operatora * 
* Arguemnty:
* - matrix1 - macierz, którą mnożymy
* - matrix2 - macierz, przez którą mnożymy
* Zwraca:
* Macierz, która jest wynikiem mnożenia.
*/
template <typename Type, int Size>
Matrix<Type, Size> operator*(const Matrix<Type, Size> &matrix1, const Matrix<Type, Size> &matrix2)
{
    Matrix<Type, Size> result;
    Type tmp;

    for (int i = 0; i < Size; i++)
    {
        for (int j = 0; j < Size; j++)
        {
            set(tmp,0);
            for (int k = 0; k < Size; k++)
            {
                tmp += matrix1(i,k) * matrix2(k,j);
            }
            result(i,j) = tmp;
        }
        
    }
    return result;
}