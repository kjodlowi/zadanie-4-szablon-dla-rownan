#pragma once
#include <iostream>

/*!
 * Modeluje pojecie liczby zespolonej
 */
struct Complex
{
    double re; /*! Pole repezentuje czesc rzeczywista. */
    double im; /*! Pole repezentuje czesc urojona. */

    Complex(){re = im = 0;} /* stworzenie struktury typu Complex o zerowej wartości części rzeczywistej i urojonej */
    Complex(double x, double y=0){re = x; im = y;} /* stworzenie struktury typu Complex o dowolnej wartości rzeczywistej i urojonej */
};

/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    arg1 - pierwszy skladnik dodawania,
 *    arg2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */

Complex operator+(const Complex arg1, const Complex arg2);

/*!
 * Realizuje odejmowanie liczb zespolonych
 * Argumenty:
 *    arg1 - pierwszy skladnik odejmowania
 *    arg2 - drugi skladnik odejmowania
 * Zwraca:
 *     Roznice dwoch skladnikow.
 */
Complex operator-(const Complex arg1, const Complex arg2);

/**
 * Funkcja, zwracająca wartość typu Complex, która jest
 * wynikiem mnożenia dwóch zmiennych typu Complex.
*/
Complex operator*(const Complex arg1, const Complex arg2);

/*
* Realizuje dzielenie dwoch liczb zespolonych arg1 przez arg2
* Przed dzieleniem sprawdza, czy arg2 nie jest rowny zero.
*/
Complex operator/(const Complex arg1, const Complex arg2);

/*!
 * Realizuje dzielenie liczby zespolonej przez rzeczywista
 * Przed dzieleniem sprawdza, czy liczba t nie jest rowna 0.
 * Argumenty:
 *    arg1 - liczba zespolona
 *    t - liczba rzeczywista
 * Zwraca:
 *    Wynik dzielenia arg1 przez t
 */
Complex operator/(const Complex arg1, const double t); /* operator dzielenia liczby zespolonej przez rzeczywista */

/* Funkcja wyświetlająca liczbę zespoloną arg1 */
void CDisplay(const Complex arg1);

/* przeciążenia operatora <<, wyświetlanie liczby zespolonej arg1 */
std::ostream& operator<< (std::ostream& StrmOut, const Complex arg1); /* operator wyswietlania wyrazen */

/*
*  Funkcja porownujaca liczby zespolone arg1 i arg2
*  Argumenty: arg1, arg2 - liczby zespolone, ktore chcemy porownac
*/
bool operator==(const Complex arg1, const Complex arg2);

/* Zwraca sprzezenie liczby zespolonej arg1 */
Complex conjugate(const Complex arg1); 

/* funkcja wykonująca moduł liczby zespolonej arg1 */
double mod1(const Complex arg1); 

/*!
 * Realizuje wczytywanie liczby zespolonej.
 * Argumenty:
 *    -plik - wskaźnik na plik, z którego czytamy liczby zespolone
 * Zwraca:
 *    Liczbe zespolona i informacje o bledzie jesli wystapil przy wczytywaniu liczby zespolonej.
 */
Complex Cread(FILE *plik);

/* Przeciazenie operatora >>, wczytywanie liczby zespolonej */
std::istream& operator>> (std::istream & StrmIn, Complex &arg1); 

/* Zwraca argument liczby zespolonej lub informację o tym, że jest on nieokreślony. */
double arg(Complex arg1);

/* Przeciążenie operatora -= przypisuje arg1 wartość arg1 - arg2 */
Complex operator-= (Complex & arg1, const Complex arg2);

/* Przeciążenie operatora += przypisuje arg1 wartość arg1 + arg2 */
Complex operator+= (Complex & arg1, const Complex arg2);

/* Przeciążenie operatora *= przypisuje arg1 wartość arg1 * arg2 */
Complex operator*= (Complex & arg1, const Complex arg2);

/* Przeciążenie operatora !=, porównuje liczbę zespoloną z liczbą typu double */
bool operator!=(const Complex arg1, const double t);

/* Przeciążenie operatora ==, porównuje liczbę zespoloną z liczbą typu double */
bool operator==(const Complex arg1, const double t);

/* Funkcja zerująca liczbę zespoloną */
Complex set(Complex & arg, const double t);

/* Funkcja zerująca liczbę typu double */
double set(double & arg, const double t);