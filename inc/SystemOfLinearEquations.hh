#pragma once
#include <cmath>
#include <iostream>
#include <iomanip>
#include "Matrix.hh"
#include "Vector.hh"
#include "Complex.hh"
 

/*
 * Klasa SystemOfLinearEuations sklada sie z:
 *  macierzy - matrix - przechowuje wprowadzona w postaci transponowanej macierz
 *  wektora - result - przechowuje wektor z rozwiazaniem rownan
 *  wektora - vector - przechowuje wektor wyrazow wolnych b
 *  wektora - err - wektor bledu
 * 
 * Przeciazone sa operatory << i >> pozwalajace zapisywac i wyswietlac uklad rownan liniowych.
 * Klasa posiada metody:
 * - gauss(SystemOfLinearEquations) - zwraca uklad rownan sprowadzony do postaci schodkowej
 * - solve(SystemOfLinearEquations) - zwraca uklad rownan z rozwiazaniem zapisanym w wektorze result
 * - err(SystemOfLinearEquations ) - zwraca uklad rownan z wektorem bledu zapisanym w wektorze err
 */

template <typename Type, int Size>
class SystemOfLinearEquations
{  
public: 
  Matrix<Type, Size> matrix;
  Vector<Type, Size> result;
  Vector<Type, Size> vector;
  Vector<Type, Size> err;
};

/*! Przeciazenie operatora >>, wczytujace liniowy uklad rownan
 * Argumenty:
 * - stream - strumien wejsciowy
 * - system - uklad rownan, gdzie zapisujemy odczyt ze strumienia wejscia
 */
template <typename Type, int Size>
std::istream &operator>>(std::istream &stream, SystemOfLinearEquations<Type, Size> &system)
{
    std::cin >> system.matrix >> system.vector;
    
    return stream;
}

/*
* Przeciążenie operatora <<, wyświetla macierz w postaci transponowanej, wektor wyrazów wolnych,
* rozwiązanie układu oraz wektor błędu powstały przy obliczeniach.
*/
template <typename Type, int Size>
std::ostream &operator<<(std::ostream &stream, SystemOfLinearEquations<Type, Size> &system)
{
    std::cout << "Macierz A^T:" << std::fixed << std::setprecision(2) 
        << std::endl
        << system.matrix
        << std::endl
        << "Wektor wyrazow wolnych b:" 
        << std::endl
        << system.vector
        << std::endl 
        << std::endl 
        << "Rozwiazanie:" 
        << std::endl
        << system.result
        << std::endl
        << std::endl
        << "Wektor bledu: " << std::scientific << system.err
        << std::endl;

    return stream;
}

/*! Funkcja sprowadzajaca macierz do postaci schodkowej
* Agrumenty:
* - matrix - macierz, ktora chcemy sprowadzic do postaci schodkowej
* Zwraca:
* - macierz w postaci schodkowej;
*/
template <typename Type, int Size>
SystemOfLinearEquations<Type, Size> gauss(SystemOfLinearEquations<Type, Size> system)
{
    int n = 0, m = 0, j = 1;
    Type tmp,vec;

    do 
    {
        if (system.matrix(n,n) != 0)
        {
            j = 1;         

            while (n + j < Size)
            {
                tmp = system.matrix(n+j,n) / system.matrix(n,n);
                while (m < Size)
                {
                    system.matrix(n+j,m) -= system.matrix(n,m) * tmp;
                    m++;
                }
                system.vector(n+j) -= system.vector(n) * tmp;
                m = 0;
                j++;
            }

            n++;
            j = 1;
            m = 0;
        }else
        {

            vec = system.vector(n);
            system.vector(n) = system.vector(n+j);
            system.vector(n+j) = vec;
            while (m < Size)
            {
                tmp = system.matrix(n,m);
                system.matrix(n,m) = system.matrix(n+j,m);
                system.matrix(n+j,m) = tmp;

                m++;
            }
            j++;
            if (n+j >= Size)
            {
                n++;
            }
        }
    }while (n < Size);

    return system;
}

/*! Funkcja rozwiązująca liniowy uklad rownan metoda Gaussa.
* Argumenty:
* - system - uklad rownan, ktory chcemy rozwiazac
* Zwraca: 
* - ten sam uklad rownan, z uzupelnionym wektorem przechowujacym rozwiazanie ukladu - result
*/
template <typename Type, int Size>
SystemOfLinearEquations<Type, Size> solve(SystemOfLinearEquations<Type, Size> system)
{
    SystemOfLinearEquations<Type, Size> tmp;  
    int j;

    tmp = gauss(system);
    tmp.result = tmp.vector;

    for (int i = Size-1 ; i > -1; i--)
    {
        for (j = Size-1; j > i; j--)
        {
            tmp.result(i) -=  tmp.matrix(i,j) * tmp.result(j);
        }
        tmp.result(i) = tmp.result(i)/tmp.matrix(i,j); 
    }
    system.result = tmp.result;

    system.err = system.matrix*system.result - system.vector;

    return system;
}