#ifndef SVECTOR_HH
#define SVECTOR_HH

#include "Complex.hh"
#include <iostream>
#include <iomanip>


/*
* Klasa Vector modeluje wektor składający się z Size elementow.
* Wektor jest zbudowany na prywatnej tablicy typu Type o rozmiarze Size.
* Dostęp do tablicy _tab[] odbywa się przez przeciezenie
* operatora (), co pozwala na zapisywanie i odczytywanie danych.
*/
template <typename Type, int Size>
class Vector
{
  Type _tab[Size];

public:
  Type operator()(unsigned int i) const { return _tab[i]; }
  Type &operator()(unsigned int i) { return _tab[i]; }
};

/*! Przeciążenie operatora >>
* Agrumenty:
* -stream - strumień, z którego wczytujemy dane
* -vec - wektor, do którego wczytujemy dane
*
* Zapisuje dane w postaci zmiennych typu dobule do tablicy 
* wektora vec.
*/
template <typename Type, int Size>
std::istream &operator>>(std::istream &stream, Vector<Type, Size> &vec)
{
  for (unsigned int i = 0; i < Size; i++)
  {
    std::cin >> vec(i);
  }
  return stream;
}

/*! Przeciążenie operatora <<
* Agrumenty:
* -stream - strumień, na który wypisujemy dane
* -vec - wektor, na który wypisujemy dane
*
* Wypisuje zmienne z tablicy wektora vec na strumień stream. 
*/
template <typename Type, int Size>
std::ostream &operator<<(std::ostream &stream, const Vector<Type, Size> &vec)
{
  for (unsigned int i = 0; i < Size; i++)
  {
    stream << vec(i) << " ";
  }
  return stream;
}

/*! Przeciążenie operatora +
* Agrumenty:
* - vec1, vec2 - wektory, które dodajemy
*
* Zwraca wektor, który jest sumą wektorów vec1 i vec2
*/
template <typename Type, int Size>
Vector<Type,Size> operator+(const Vector<Type, Size> &vec1, const Vector<Type,Size> &vec2)
{
  Vector<Type,Size> result;

  for (unsigned int i = 0; i < Size; i++)
  {
    result(i) = vec1(i) + vec2(i);
  }
  return result;
}

/*! Przeciążenie operatora -
* Agrumenty:
* - vec1, vec2 - wektory, które odejmujemy
*
* Zwraca wektor, który jest sumą wektorów vec1 i -vec2
*/
template <typename Type, int Size>
Vector<Type,Size> operator-(const Vector<Type,Size> &vec1, const Vector<Type,Size> &vec2)
{
    Vector<Type,Size> result;

    for (int i = 0; i < Size; i++)
    {
        result(i) = vec1(i) - vec2(i);
    }
    return result;
}

/*! Przeciążenie operatora *
* Agrumenty:
* - vec1, vec2 - wektory, które mnożymy
*
* Zwraca liczbę typu Type, która jest
* iloczynem skalarnym wektorow vec1, vec2.
*/
template <typename Type, int Size>
Type operator*(const Vector<Type,Size> &vec1, const Vector<Type,Size> &vec2)
{
    Type result(0);

    for (int i = 0; i < Size; i++)
    {
        result += vec1(i) * vec2(i);
    }
    return result;
}

/*! Przeciążenie operatora *
* Agrumenty:
* - vec1 - wektor który mnożymy przez liczbę
* - t - zmienna typu float, przez którą mnożymy wektor
*
* Zwraca wektor vec1, przemnożony przez liczbe t;
*/
template <typename Type, int Size>
Vector<Type,Size> operator*(const Vector<Type,Size> &vec, const float t)
{
    Vector<Type,Size> result;
    
    for (int i = 0; i < Size; i++)
    {
        result(i) = vec(i) * t;
    } 
    return result;
}

/*! Przeciążenie operatora /
* Agrumenty:
* - vec1 - wektor który dzielimy przez liczbę
* - t - zmienna typu float, przez którą dzielimy wektor
*
* Zwraca wektor vec1, podzielony przez liczbe t;
*/
template <typename Type, int Size>
Vector<Type,Size> operator/(const Vector<Type,Size> &vec, const float t)
{
    Vector<Type,Size> result;
    
    for (int i = 0; i < Size; i++)
    {
        result(i) = vec(i) / t;
    }
    return result;
}

#endif