#include <iostream>
#include "Vector.hh"
#include "Matrix.hh"
#include "Size.hh"
#include "SystemOfLinearEquations.hh"

using namespace std;


/* Modyfikacja
 * Funkcja testujaca dzialanie jednej z wlasnosci liczb zespolonych
 */
Matrix<double,2> Complex_to_Matrix(Complex c)
{
    Matrix<double,2> result;
    result(0,0) =  c.re;
    result(1,0) =  -c.im;
    result(0,1) =  c.im;
    result(1,1) =  c.re;
    return result;
}

/* 
 * Funkcja, która wywołuje funkcję rozwiązującą układ równań typu double
 * oraz wyświetla jego rozwiązanie.
 */
void SoE_double()
{
    SystemOfLinearEquations<double,SIZE> system_of_equations;
    cin >> system_of_equations;
    system_of_equations = solve(system_of_equations);
    cout << system_of_equations;
}

/* 
 * Funkcja, która wywołuje funkcję rozwiązującą układ równań typu Complex
 * oraz wyświetla jego rozwiązanie.
 */
void SoE_Complex()
{
    SystemOfLinearEquations<Complex,SIZE> system_of_equations;
    cin >> system_of_equations;
    system_of_equations = solve(system_of_equations);
    cout << system_of_equations;
}

int main()
{
    
    char c = ' ';
    while(c == ' ' || c == 10){c = fgetc(stdin);}
    ungetc(c,stdin);
    cin >> c;

    switch (c)
    {
    case 'r':
        SoE_double();
        break;

    case 'z':
        SoE_Complex();
        break;
    
    default:
    cout << "We wprowadzonych danych nie podano parametru 'r' lub 'z', okreslajacego typ danych! (r - rzeczywiste, z - zespolone)" << endl;
        break;
    }
    
    /*

    Complex test;
    Matrix<double,2> mat;

    cin >> test;

    mat = Complex_to_Matrix(test);

    cout << endl << "liczba zespolona w postaci macierzy: " << endl << mat << endl;
    cout << "wyznacznik macierzy: " << determinant(mat) << endl
    << "mod lzesp: " << mod1(test)*mod1(test) << endl;
    
    */
}
